<!DOCTYPE html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta http-equiv="keywords" content="Git, Version Control" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="style.css">
    <link href="https://fonts.googleapis.com/css?family=Lobster" rel="stylesheet" type="text/css">
    <script src="js/script.js" type="text/javascript"></script>
</head>
<nav id="navbar">
    <header>Basics for Git with Gitlab</header>
    <ul>
        <li><a class="nav-link" href="#Configure_Git">Configure Git</a></li>
        <li><a class="nav-link" href="#Git_Authentication_Methods">Git Authentication Methods</a></li>
        <li><a class="nav-link" href="#Git_Terminology">Git Terminology</a></li>
        <li><a class="nav-link" href="#Basic_Git_Commands">Basic Git Commands</a></li>
        <li><a class="nav-link" href="#More_Info">More Info</a></li>
    </ul>
</nav>
<main id="main-doc">
    <section class="main-section" id="Configure_Git">
        <header>Configure Git</header>
        <p>To start using Git from your computer, you must enter your credentials (user name and email) to identify you as the author of your work. The user name and email should match the ones you’re using on GitLab.</p>
        </p>In your shell, add your user name:  </p>
        <code class="codehighlight">git config --global user.name "your_username"</code>
        <p>And your email address: </p>
        <code class="codehighlight">git config --global user.email "your_email_address@example.com"</code>
        <p>To check the configuration, run: </p>
        <code class="codehighlight">git config --global --list</code>
        <p>The --global option tells Git to always use this information for anything you do on your system. If you omit --global or use --local, the configuration is applied only to the current repository.</p>
        <p>You can read more on how Git manages configurations in the <a href="https://git-scm.com/book/en/v2/Customizing-Git-Git-Configuration">Git configuration documentation</a>.</p>
    </section>
    <br>
    <section class="main-section" id="Git_Authentication_Methods">
        <header>Git Authentication Methods</header>
        <p>To connect your computer with GitLab, you need to add your credentials to identify yourself. You have two options: </p>
        <ul>
            <li>Authenticate on a project-by-project basis through HTTPS, and enter your credentials every time you perform an operation between your computer and GitLab.</li>
            <li>Authenticate through SSH once and GitLab no longer requests your credentials every time you pull, push, and clone.</li>
        </ul>
        <p>To start the authentication process, we’ll clone an existing repository to our computer:</p>
        <ul>
            <li>If you want to use <strong>SSH</strong> to authenticate, follow the instructions on the SSH documentation to set it up before cloning.</li>
            <li>If you want to use <strong>HTTPS</strong>, GitLab requests your user name and password.</li>
                <ul>
                    <li> If you have 2FA enabled for your account, you must use a <a href="https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html">Personal Access Token</a> with <strong>read_repository</strong> or <strong>write_repository</strong> permissions instead of your account’s password. Create one before cloning.</li>
                    <li> If you don’t have 2FA enabled, use your account’s password.</li>
                </ul>
        </ul>
        <p>Authenticating via SSH is the GitLab recommended method. You can read more about credential storage in the <a href="https://git-scm.com/book/en/v2/Git-Tools-Credential-Storage">Git Credentials documentation</a>.</p>
    </section>
    <br>
    <section class="main-section" id="Git_Terminology">
        <header>Git Terminology</header>
        <p>If you’re familiar with the Git terminology, you may want to jump directly into the <a href="#Basic_Git_Commands">Basic Git_Commands</a>.</p>
        <h3>Namespace</h3>
        <p>A <strong>namespace</strong> is either a <strong>user name</strong> or a <strong>group name</strong>. For example, suppose Jo is a GitLab.com user and they chose their user name as jo. You can see Jo’s profile at https://gitlab.com/jo. jo is a namespace. Jo also created a group in GitLab, and chose the path test-group for their group. The group can be accessed under https://gitlab.com/test-group. test-group is a namespace.</p>
        <h3>Repository</h3>
        <p>Your files in GitLab live in a <strong>repository</strong>, similar to how you have them in a folder or directory in your computer. <strong>Remote</strong> repository refers to the files in GitLab and the copy in your computer is called <strong>local</strong> copy. A <strong>project</strong> in GitLab is what holds a repository, which holds your files. Often, the word “repository” is shortened to “repo”.</p>
        <h3>Fork</h3>
        <p>When you want to copy someone else’s repository, you <strong>fork</strong> the project. By forking it, you create a copy of the project into your own namespace to have read and write permissions to modify the project files and settings.</p>
        <p>For example, if you fork this project, https://gitlab.com/gitlab-tests/sample-project/ into your namespace, you create your own copy of the repository in your namespace (https://gitlab.com/your-namespace/sample-project/). From there, you can clone it into your computer, work on its files, and (optionally) submit proposed changes back to the original repository if you’d like.</p>
        <h3>Download vs clone</h3>
        <p>To create a copy of a remote repository’s files on your computer, you can either download or clone. If you download, you cannot sync it with the remote repository on GitLab.</p>
<p>Cloning a repository is the same as downloading, except it preserves the Git connection with the remote repository. This allows you to modify the files locally and upload the changes to the remote repository on GitLab.</p>
    <h3>Pull and Push</h3>
    <p>After you saved a local copy of a repository and modified its files on your computer, you can upload the changes to GitLab. This is referred to as <strong>pushing</strong> to GitLab, as this is achieved by the command <code class="codehighlight">git push</code>.</p> 
    <p>When the remote repository changes, your local copy is behind it. You can update it with the new changes in the remote repository. This is referred to as <strong>pulling</strong> from GitLab, as this is achieved by the command <code class="codehighlight">git pull</code>.</p>
    </section>
    <br>
    <section class="main-section" id="Basic_Git_Commands">
        <header>Basic Git Commands</header>
        <p>For the purposes of this guide, we use this example project on GitLab.com: <a href="https://gitlab.com/gitlab-tests/sample-project/">https://gitlab.com/gitlab-tests/sample-project/</a>.</p>
        <p>To use it, log into GitLab.com and fork the example project into your namespace to have your own copy to playing with. Your sample project is available under <code>https://gitlab.com/<your-namespace>/sample-project/</code>.</p>
        <p>You can also choose any other project to follow this guide. Then, replace the example URLs with your own project’s.</p>
        <p>If you want to start by copying an existing GitLab repository onto your computer, see how to clone a repository. On the other hand, if you want to start by uploading an existing folder from your computer to GitLab, see how to convert a local folder into a Git repository.</p>
        <h3 id="clone_a_repository">Clone a repository</h3>
        <p>To start working locally on an existing remote repository, clone it with the command <code>git clone <repository path></code>. You can either clone it via <a href="#clone_via_https">HTTPS</a> or <a href="#clone_via_ssh">SSH</a>, according to your preferred <a href="#git_authentication_methods">authentication method</a>.</p>
        <p>You can find both paths (HTTPS and SSH) by navigating to your project’s landing page and clicking Clone. GitLab prompts you with both paths, from which you can copy and paste in your command line. You can also clone and open directly in Visual Studio Code.</p>
        <p>For example, considering our sample project:</p>
        <ul>
            <li>To clone through HTTPS, use <code>https://gitlab.com/gitlab-tests/sample-project.git</code>.</li>
            <li>To clone through SSH, use <code>git@gitlab.com:gitlab-tests/sample--project.git</code>.</li>
        </ul>
        <p>To get started, open a terminal window in the directory you wish to add the repository files into, and run one of the git clone commands as described below.</p>
        <p>Both commands download a copy of the files in a folder named after the project’s name and preserve the connection with the remote repository. You can then navigate to the new directory with cd sample-project and start working on it locally.</p>
        <h3 id="Clone_via_HTTPS">Clone via HTTPS</h3>
        <p>To clone <code>https://gitlab.com/gitlab-tests/sample-project/</code> via HTTPS:</p>
        <code class="codehighlight">git clone https://gitlab.com/gitlab-tests/sample-project.git</code>
        <p>On Windows, if you entered incorrect passwords multiple times and GitLab is responding <code>Access denied</code>, you may have to add your namespace (user name or group name) to clone through HTTPS: <code>git clone https://namespace@gitlab.com/gitlab-org/gitlab.git</code>.</p>
        <h3 id="Clone_via_SSH">Clone via SSH</h3>
        <p>To clone <code>https://gitlab.com/gitlab-tests/sample-project/</code> via SSH:</p>
        <code class="codehighlight">git clone git@gitlab.com:gitlab-tests/sample-project.git</code>
    </section>
    <br>
    <section class="main-section" id="More_Info"> 
        <header>More Info</header>
        <p>This is a summary of content sourced from the Gitlab Documentation. For much more in-depth instructions, see <a href="https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html">this gitlab Getting Started page</a>. It's license is <a href="https://creativecommons.org/licenses/by-sa/4.0/">CC BY-SA</a> (Attribution-ShareAlike).
        </p>
    </section>
    <script src="https://cdn.freecodecamp.org/testable-projects-fcc/v1/bundle.js"></script>
</main>
